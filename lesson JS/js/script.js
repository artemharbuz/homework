// let - можно изменять значение в строке console например как в примере
// let userAge = 10;
// userAge = 15;
// console.log(userAge);
// let userName = "Danil";
// console.log(userName = "Artem")

// confirm возврощвет булевое значение


// ==	Равно (проверяет равно ли одно значение другому, не путайте с оператором присваивания "=")
/*const user = 10 == 10; - true
console.log(user);*/

/*const user = 10 == "10"; - true
console.log(user);*/

// ===	Строгое равно - проверяет равно ли одно значение другому без приведения типа

/*const user = 10 === 10; - true
console.log(user);*/

/*const user = 10 === "10"; - false
console.log(user);*/


// const - нельзя изменять значение, использовать в случаях когда менять ничего не нужно пример
// const userAge = 10;
// console.log(userAge);

// Имена констант с заранее известными значениями
// ПРИМЕР:
// const BLOCK_SIZE = 25;
// const COLOR_RED = "#F00";

// Имя констант со значениями присвоенными в процессе выполнения программы
// const summSizes = 25 + 30;

// Кавычки Одтнарные, двойные и обратные. одинарные и двойные идентичные.Одтнарные.Одтнарные.
// let userName = "Artem";
// let userAge = 25;
// const myInfo = " Імя: ${userName}, Вік: ${userAge}";
// console.log(myInfo)

// Тип

// Основные типы данных
// undefined

// undefined- неопределенный тип 
// когда переменной небыло задано значения на экране будет undefined

// null-содержит только одно значение null
// ПРИМЕР:
/*let age = null;
console.log(age);*/

// number- Числовой тип данных представляет значения как в виде целых чисел
// так и в виде чисел с плавающей точкой

// Nan когда допущена математическая ошибка

// boolean - правда или ложь 
// ПРИМЕРИ:
// const someVar = 10 > 5;
// console.log(someVar); ответ TRUE

// const someVar = 10 < 10;
// console.log(someVar); ответ FALSE

// let someVar = 10 < 15;
// console.log(someVar = 10 > 15); пример с let

// Массиви
// Ряд это массив
// let someString = "Privet";
// console.log(someString[0]); на экране бука P,начинается слово с ноля

// Поиск - методы includes наиболее используемый , startswith, endswith

// includes - пример
// let someString = "Privet";
// console.log(someString.includes("p")); - на экране получим false так как буквы p нижегп регистра в нашем слове нет;

// let someString = "Privet";
//  console.log(someString.includes("P")); - получаем true так как буква с таким регистром найдена в нашем слове.

// endswith-Пример
// let someString = "Privet";
// console.log(someString.endsWith("t")); получаем true так как слово заканчивается буквой t

// startswith - пример
// let someString = "Artem";
// console.log(someString.startsWith("A")); Получаем true если наша буква такая как и начинается Имя если напишем любую другую будет false:

// Поиск и замена replace
// let someString = "Pivet Artem"
// someString = someString.replace("Pivet", "Den Dobry");
// console.log(someString); при помощи этой записи мы можем сделать замену буквы, цыфры, предложений

// Основные методы чисел

// Math.floor - округление числа в меньшую сторону
// ПРИМЕР: 
// let someNotRoundNum = 25.6;
// console.log(Math.floor(someNotRoundNum)); - свойство нужно записывть с большой буквы иначе не будет работать

// Math.ceil - округление в большую сторону
// ПРИМЕР:
// let someNotRoundNum = 25.2;
// console.log(Math.ceil(someNotRoundNum));

// Math.round - округляет по принципу если цифра после точки (например 25.4) не привышает 4 тогда число округлится в меньшую сторону, все что .5 и выше округляется в большую сторону
// ПРИМЕР:
// let someNotRoundNum = 25.4;
// console.log(Math.round(someNotRoundNum));
// let someNotRoundNum = 25.5;
// console.log(Math.round(someNotRoundNum))

// Math.abs - если число отрицательное, отбрасывается минус и остается положительное
// ПРИМЕР:
// let someNotRoundNum = 25 - 30;
// console.log(Math.abs(someNotRoundNum));

// Math.random - выподает рандомное число если вписать 0
// let someNotRoundNum = 0;
// console.log(Math.random(someNotRoundNum));

// Math.max(min)- находит максимальноеи минимальнре число

// parseInt и parsefloat - получим в первом случает целое число, во втором такое какое оно и есть
// ПРИМЕРЫ:

// let someVar = 129.9;
// console.log(parseInt(someVar)); -получим на экран 129

// let someVar = 129.9;
// console.log(parseFloat(someVar)); - получаем на экран число 129.9 

// Основные операторы

// -Оператор минус
   // let varOne = 12;
   // let varTwo = 15;
   // let varThree = 1;
   // let varSumm = varOne - varTwo - varThree;
   // console.log(varSumm)
// -Оператор деления
   // let varOne = 100;
   // let varTwo = 2;
   // let varSumm = varOne / varTwo;
   // console.log(varSumm);

// -Оператор умножения
   // let varOne = 2;
   // let varTwo = 4;
   // let varThree = 10;
   // let varFour = 2
   // let varSumm = varOne * varTwo * varThree * varFour;
   // console.log(varSumm)
// -Оператор взятие остатка от деления
   // пересмотреть по этому вопросу
// -Унарный оператор сложения
   // let someNum = 5;
   // let result = +someNum; - вписав один + получим тоже число что и было
   // console.log(result);

   // let someNum = 5;
   // let result = ++someNum; - вписав два + получим число на одну еденицу больше от указанного
   // console.log(result);

   // let someNum = 5;
   // let result = --someNum; - вписав два - получим числр на одну еденицу меньше
   // console.log(result);

// Приоритетность операторов

// -Операторы сравнения
   // let result = 10 > 5;
   // console.log(result); на экране true так как ответ верный

   // let result = 10 < 8; на экране False
   // console.log(result);
// -Операторы логичные
   // !-Оператор "НЕТ"
   // ПРИМЕР:
   /* alert( !true ); // false
   alert( !0 ); // true   ---  0 всегда false*/

  /*  В частности, двойное НЕ используют для преобразования значений к логическому типу:

   alert( !!"non-empty string" ); // true
   alert( !!null ); // false
   То есть первое НЕ преобразует значение в логическое значение и возвращает обратное, а второе НЕ снова инвертирует его. В конце мы имеем простое преобразование значения в логическое.*/

   /*Есть немного более подробный способ сделать то же самое – встроенная функция Boolean:

   alert( Boolean("non-empty string") ); // true
   alert( Boolean(null) ); // false*/

   //  let result = !0;
   // console.log(Boolean(result))
   // ||- Оператор или
   // let result = 10 > 20 || 20 < 10 || 35 === 35; -true 
   // console.log(result);
   /* Если значение не логического типа, то оно к нему приводится в целях вычислений.
      Например, число 1 будет воспринято как true, а 0 – как false:
      if (0 || 2) {
      alert( 'truthy!' );
   }*/
   /*let closed = 9;
   if (closed < 10 || closed === 9){
      alert("магазин закрыт");
   }*/
   /*let x;

   false || (x = 1);

   alert(x); // 1*/

   // && - Оператор и

    /*let result = 10 > 20 && 20 < 10; - false
   console.log(result); */


   /*let result = 10 < 20 && 20 < 10; - false
   console.log(result); */

   /*let result = 10 < 20 && 20 > 10; - true
   console.log(result); */

   /*let hour = 15;
   let minute = 47;

   if(hour === 15 &&  minute === 47) {
      alert("day off")
   }*/

   /*if (1 && 0) { // вычисляется как true && false
      alert( "не сработает, так как результат ложный" );
    }*/

   /* Можно передать несколько значений подряд. В таком случае возвратится первое «ложное» значение, на котором остановились вычисления.
   alert( 1 && 2 && null && 3 ); // null*/

   /*Когда все значения верны, возвращается последнее

   alert( 1 && 2 && 3 ); // 3*/

   /*Приоритет оператора && больше, чем у ||
   Приоритет оператора И && больше, чем ИЛИ ||, так что он выполняется раньше.
   Таким образом, код a && b || c && d по существу такой же, как если бы выражения && были в круглых скобках: (a && b) || (c && d).3*/
   /*let x = 3;
   (x > 2) && alert( 'Greater than zero!' ); - если первое сравнение true, в таком случаем вычисление будет происходить в правой части*/

   /*let a = +prompt("Сколько будет 5 + 5?");
    switch (a) {
      case 2:
         alert('Очень мало');
         break;
      case 10:
         alert('в точку');
         break; 
      case 9:
         alert('Попробуй еще раз');
         break;
         default:
            alert('Нет таких значений');
    }*/
     

   // Условия выветвления строится с помощью if(умова){выполняется в том случае если вернет true}
// если добавить до фигурных скобок else{выполнится в том случае тесли вернет false}
// else if если ровняется
// ПРИМЕР:
   //   let varOne = 10;
   //   let varTwo = 5;
   //   if(varOne < varTwo){let result = varOne + varTwo;
   //   console.log(result);}else if(varOne === 10){
   // console.log("varOne  = 5")}

// Цикл for
// let someString = "Privet!"
//  for(начало; условия окончания; шаг){}
//  код выполняется на каждом поле цикла
// ПРИВЕТ:
//   let someString = "Privet!"
//   for(let i=0;i<someString.length; ++i)
// {console.log(someString[i]);}

// while
/*Цикл “while”
Цикл while має такий синтаксис:

while (умова) {
  // код
  // так зване "тіло циклу"
}*/
/*Доки умова є вірною, виконується код із тіла циклу.

Наприклад, цикл нижче виводить i поки i < 3:

let i = 0;
while (i < 3) { // показується 0, далі 1, потім 2 
  alert( i );
  i++;
}*/
/*Одне виконання циклу називається ітерацією. Цикл в зразку вище робить три ітерації.*/ 

/*let i = 10;
while (i) { // коли i буде 0, умова стане невірною, і цикл зупиниться
  alert( i );
  i--;
}*/

/*for (let i=0; i<5; i++) alert(i)*/


/*let number = +prompt("number?");
while (number === 3);
  console.log("Cool number");
  number = +prompt("number?");*/

// do while loop
/*let number;
do{
  number = +prompt("number?");
  console.log("cool number")
} while (number === 3);*/
/*Label1: for(let i = 0; i < 500; i++){
   console.log(i);
   if(i > 455){
      continue Label1;
   }
   console.log("end loop");
}
console.log("end label loop")*/


// Условный оператор
// const num = +prompt("Your number is?");
// let numDescription = "";
/*if(num < 0){
   alert("Negative number")
}

if(num === 0){
   alert("Number is 0")
}

if(num > 0){
   alert("positive number")
}*/
/*if(num < 0){
   numDescription = "Negative number";
} else if(num === 0){
  numDescription = "Number is 0";
} else {
   numDescription = "positive number";
}
console.log(numDescription);*/
// тернарный оператор
// numDescription = (num < 0) ? "Negative number" : (num === 0) ? "Number is 0" : "positive number";
// console.log(numDescription);

/*ДЗ-1
"use strict"
console.log(" Я Учу JS");*/

// ДЗ-2
// 1) Прижумать имя переменной для хранения цвета глаз
   //  let colorEye

 /*2) Объявить две переменных, user и userName.
     Присвоить значения Вася в переменную userName.
     скопировать значение и присвоить переменной user.
     Вывести на экран переменную user со значением Вася*/
/* "use strict"
let userName = "Вася";
user = userName;
console.log(user);*/

// ДЗ
// 1) let или const дальше имя переменной
// 2) confirm спрашивает во всмплывающем окне
      // prompt собирает информацию от пользователя
   //  3) console.log(4 * "2"); неявное преобразование потому что число 2 записано как строка
  /*4) "use strict"
   let admin = "Artem"
   let name = "Artem"
   console.log(admin);*/


/*let days;
days = 5;
const second = days * 24 * 60 * 60;
console.log(second);*/
// let text = prompt("Your age", "18");- Выведет модальное окно с текстом "Your age", полем для ввода данных и значением в нем "18", результат ввода сохранит в переменную text

/*const number = Math.floor(Math.random() * 6) + 1
const userName = +prompt("загадайте число от 1 до 6")
if (number < userName) {
   alert("меньше!")
} else if (number === userName) {
   alert("поздравляем")
} else {
   alert("больше")
}*/
/*let = num;

do {
  num = prompt("Введене число, більше за 100?", 0);
} while (num <= 100 && num);*/

/*Задача 1
Напишите программу, которая выводит в консоль все числа от 100 до 0, которые делятся на 5 без остатка, в обратном порядке. Пример вывода:

for(let i = 100; i >= 0; i -= 5) {
   console.log(count);
 }*/


/*for(i = 0; i < 10; i++) alert(i);*/

// на экране 2 4 6 8 10
/*for(let i = 2; i <= 10; i++) {
   if(i % 2 == 0){
    alert(i);
}
}*/



/*Задача 2
let count = 0;
let i;

do {
  i = Math.random()
  count++;
} while(i < 0.9)

console.log('Случайное число: ' + i);
console.log('Попыток: ' + count);*/

/*let user = +prompt('Enter number');

if (Number.isNaN(user)) {
   user = +prompt('Введите пожалуйста число');
}

if (user % 2 == 0) {
   console.log('Ваше число четное');
} else if (user % 2 == 1) {
   console.log('Ваше число не четное');
} else {
   alert('Ошибка вы ввели не верно');
}*/

// Вариант if else
/*let user = prompt('Введите свое имя');

if(user == 'Mike'){
   console.log(`Привет СЕО ${user}`);
}else if(user == 'Jane'){
   console.log(`Привет СТО ${user}`);
}else if(user == 'Walter'){
   console.log(`Привет программист ${user}`);
}else if(user == 'Oliver'){
   console.log(`Привет менеджер ${user}`);
}else if(user == 'John'){
   console.log(`Привет уборщик ${user}`);
}else{
   console.log(`Пользователь не найден ${user}`);
}*/


// Вариант switch
/*switch(user){
   case 'Mike': {
      alert(`Привет СЕО  ${user}`);
      break;
   }
   case 'Jane': {
      alert(`Привет СТО ${user}`);
      break;
   }
   case 'Walter': {
      alert(`Привет программист ${user}`);
      break;
   }
   case 'Oliver': {
      alert(`Привет менеджер ${user}`);
      break;
   }
   case 'John': {
      alert(`Привет уборщик ${user}`);
      break;
   }
   
   default:{
      alert(`Пользователь не найден ${user}`);
   }
}*/
let age = +prompt("Уажите ваш возраст");
const userName = prompt("Укажите ваше Имя");
if(age < 18){
   alert("You are not allowed to visit this website");
   block;
}else if(age >= 18 && age <= 22);
const userAnswer = confirm("Are you sure you want to continue?");
if(userAnswer){
   alert("Welcome " + userName);
}else{
   alert("You are not allowed to visit this website");
}
   



